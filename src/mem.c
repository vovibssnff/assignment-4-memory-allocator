#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header* block) { return block->capacity.bytes >= query; }
static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void* restrict addr, block_size block_sz, void* restrict next) {
    *((struct block_header*)addr) = (struct block_header) {
        .next = next,
        .capacity = capacity_from_size(block_sz),
        .is_free = true
    };
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region* r);

static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region (void const * addr, size_t query) {
    const size_t region_size = region_actual_size(query + offsetof(struct block_header, contents));
    void *actual_addr = map_pages(addr, region_size, MAP_FIXED);
    if (actual_addr == MAP_FAILED) {
        actual_addr = map_pages(addr, region_size, 0);
        if (actual_addr == MAP_FAILED) {
            return REGION_INVALID;
        }
    }
    struct region region = {
        .addr = actual_addr,
        .size = region_size,
        .extends = (actual_addr == addr)
    };
    block_init(region.addr, (block_size){region_size}, NULL);
    return region;
}

static void* block_after(struct block_header const* block);

static bool blocks_continuous (struct block_header const* fst, struct block_header const* snd) {
    return (void*)snd == block_after(fst);
}

void* heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;
    return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term() {
    struct block_header *block = (struct block_header *)HEAP_START;
    while (block != NULL) {
        struct block_header *clean_address = block;
        size_t length = 0;
        while (block != NULL && blocks_continuous(block, block->next)) {
            length += size_from_capacity(block->capacity).bytes;
            block = block->next;
        }
        if (block != NULL) {
            length += size_from_capacity(block->capacity).bytes;
            block = block->next;
        }
        munmap(clean_address, length);
    }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */
static bool block_splittable(struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header* block, size_t query) {
    if (!block_splittable(block, query)) {
        return false;
    }
    void* addr_1 = block;
    block_size size_1 = {.bytes = size_from_capacity((block_capacity){.bytes = query}).bytes};
    void* addr_2 = query+block->contents;
    block_size size_2 = {.bytes = block->capacity.bytes-query};
    struct block_header* next = block->next;
    block_init(addr_1, size_1, addr_2);
    block_init(addr_2, size_2, next);
    return true;
}

/*  --- Слияние соседних свободных блоков --- */
static void* block_after(struct block_header const* block) {
    return (void*)(block->contents + block->capacity.bytes);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header* block) {
    if (!block->next || !block->is_free || !block->next->is_free || !mergeable(block, block->next)) {
        return false;
    }
    block_size size = {.bytes = 2*offsetof(struct block_header, contents) +
                                block->capacity.bytes + block->next->capacity.bytes};
    block_init(block, size, block->next->next);
    return true;
}

/*  --- ... ecли размера кучи хватает --- */
struct block_search_result {
    enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
    struct block_header* block;
};

static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    struct block_search_result bsr = {.type = BSR_CORRUPTED};
    struct block_header *cur_block = block;
    struct block_header *prev_block = NULL;
    bool is_corrupted = true;
    while (cur_block != NULL) {
        is_corrupted = false;
        if (try_merge_with_next(cur_block)) {
            continue;
        }
        if (cur_block->capacity.bytes >= sz && cur_block->is_free) {
            return (struct block_search_result){BSR_FOUND_GOOD_BLOCK, cur_block};
        }
        prev_block = cur_block;
        cur_block = cur_block->next;
    }
    if (!cur_block && !is_corrupted) {
        return (struct block_search_result){BSR_REACHED_END_NOT_FOUND, prev_block};
    }
    return bsr;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header* block) {
    struct block_search_result result = find_good_or_last(block, query);
    if (result.type!=BSR_FOUND_GOOD_BLOCK) {
        return result;
    }
    split_if_too_big(result.block, query);
    result.block->is_free = false;
    return result;
}

static struct block_header* grow_heap(struct block_header* restrict last, size_t query) {
    struct region region = alloc_region(last->contents + last->capacity.bytes, query);
    last->next = region.addr;
    try_merge_with_next(last);
    struct block_header* block = (!region.extends || !last->is_free) ? region.addr : last;
    return block;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc(size_t query, struct block_header* heap_start) {
    if (query < BLOCK_MIN_CAPACITY) query = BLOCK_MIN_CAPACITY;
    struct block_search_result bsr = try_memalloc_existing(query, heap_start);
    switch (bsr.type) {
        case BSR_FOUND_GOOD_BLOCK:
            return bsr.block;
        case BSR_CORRUPTED:
            return NULL;
        case BSR_REACHED_END_NOT_FOUND: {
            struct block_header *block = grow_heap(bsr.block, query);
            bsr = try_memalloc_existing(query, block);
        }
    }
    return bsr.block;
}

void* _malloc(size_t query) {
    struct block_header* const addr = memalloc(query, (struct block_header*) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free(void* mem) {
    if (!mem) return ;
    struct block_header* header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header))
        ;
}
