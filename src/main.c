#define _GNU_SOURCE

#include "mem.h"
#include <stdio.h>

#define REGION_SIZE 4096

void debug(const char *fmt, ...);

// Тест 1: Стандартная аллокация
void test_1() {
    debug(">>>Тест стандартной аллокации");
    void* main_heap = heap_init(0);
    void* block1 = _malloc(54);
    void* block2 = _malloc(sizeof(size_t) * 54);
    void* block3 = _malloc(sizeof(int64_t));
    debug_heap(stderr, main_heap);
    _free(block1);
    _free(block2);
    _free(block3);
    debug_heap(stderr, main_heap);
    heap_term();
    debug("\n>>>Успех\n");
}

// Тест 2: Освобождение одного блока из нескольких выделенных
void test_2() {
    debug(">>>Тест освобождения одного блока");
    void* main_heap = heap_init(0);
    _malloc(sizeof(uint16_t));
    void* block = _malloc(sizeof(uint64_t));
    _malloc(sizeof(uint32_t));
    debug_heap(stderr, main_heap);
    _free(block);
    debug_heap(stderr, main_heap);
    heap_term();
    debug("\n>>>Успех\n");
}

// Тест 2: Освобождение двух блоков из выделенных
void test_3() {
    debug(">>>Тест освобождения двух блоков");
    void* main_heap = heap_init(0);
    _malloc(sizeof(uint8_t));
    _malloc(sizeof(uint16_t));
    void* block1 = _malloc(sizeof(uint32_t));
    void* block2 = _malloc(sizeof(uint64_t));
    _malloc(sizeof(uint16_t));
    debug_heap(stderr, main_heap);
    _free(block1);
    _free(block2);
    debug_heap(stderr, main_heap);
    heap_term();
    debug("\nУспех\n");
}

// Тест 4: Расширение региона
void test_4() {
    debug(">>>Тест расширения региона");
    void* main_heap = heap_init(0);
    void* block1 = _malloc(REGION_SIZE);
    debug_heap(stderr, main_heap);
    void* block2 = _malloc(REGION_SIZE);
    debug_heap(stderr, main_heap);
    void* block3 = _malloc(REGION_SIZE);
    _free(block1);
    debug_heap(stderr, main_heap);
    _free(block2);
    debug_heap(stderr, main_heap);
    _free(block3);
    debug_heap(stderr, main_heap);
    heap_term();
    debug("\nУспех\n");
}

// Тест 5: Новый регион в другом месте
void test_5() {
    debug(">>>Тест расширения региона в другой локации");
    void* main_heap = heap_init(0);
    void* blocked_block_addr = main_heap + REGION_SIZE;
    void* blocked_block = mmap(blocked_block_addr, REGION_SIZE, PROT_READ | PROT_WRITE,
                               MAP_PRIVATE | MAP_FIXED, -1, 0);
    void* block1 = _malloc(REGION_SIZE);
    debug_heap(stderr, main_heap);
    void* block2 = _malloc(REGION_SIZE);
    debug_heap(stderr, main_heap);
    _free(block1);
    debug_heap(stderr, main_heap);
    _free(block2);
    debug_heap(stderr, main_heap);
    _free(blocked_block);
    heap_term();
    debug("\nУспех\n");
}

int main() {
    test_1();
    test_2();
    test_3();
    test_4();
    test_5();
    return 0;
}
